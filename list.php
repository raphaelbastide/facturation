<?php include "functions.php"; ?>
<!DOCTYPE html>
<html>
<head>
  <title>Facta - list</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="fonts/stylesheet.css">
  <link href="css/grid.css" rel="stylesheet" />
  <link rel="shortcut icon" href="favicon.svg" type="image/x-icon">
</head>
<body data-lang="en">
<nav><button class="clients-btn" data-display="client">Clients</button> <button class="bills-btn" data-display="bill">Bills</button></nav>
<div class="client-table table" hidden></div>
<div class="bill-table table"></div>
    <script src="js/grid.js"></script>
    <script>
      document.querySelectorAll('nav button').forEach(btn => {
        btn.onclick=function(){
          document.querySelectorAll('.table').forEach(table => {
            table.setAttribute('hidden', 'true')
          });
          document.querySelector(`.${btn.dataset.display}-table`).removeAttribute('hidden')
        }
      });

      new gridjs.Grid({
        columns: ["ID", "Name", "Address", "Email", "SIRET"],
        server: {
          url: 'data.json',
          then: data => {
            return Object.keys(data).map(key => {
              const client = data[key];
              return [client.id, client.name, client.address, client.email, client.siret];
            });
          }
        },
        search:true,
        sort:true,
        style: {
          td: {
            border: '1px solid #ccc'
          },
          table: {
            'font-size': '15px'
          }
        }
      }).render(document.querySelector(".client-table"));

      new gridjs.Grid({
        columns: ["ID", "Client", "Date", "Title", "Status","Price"],
        server: {
          url: 'data.json',
          then: data => {
            const bills = [];
            Object.keys(data).forEach(key => {
              const client = data[key];
              client.bills.forEach(bill => {
                let total = 0
                bill.bill_lines.forEach(bill_line => {
                  total += bill_line.price * bill_line.amount
                });
                bills.push([bill.id, client.name,bill.date, bill.title, bill.status, total]);
              });
            });
            return bills;
          }
        },
        search: true,
        sort: true,
        style: {
          td: {
            border: '1px solid #ccc'
          },
          table: {
            'font-size': '15px'
          }
        }
      }).render(document.querySelector(".bill-table"));

  </script>
</body>
</html>
