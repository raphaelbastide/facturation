<?php
include "functions.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $clientId = $_POST['client-id'];
  if ($_POST['bill-id'] >= 0) {
    $billId = $_POST['bill-id'];
  }else{
    $billId = false;
  }
  $billTitle = $_POST['bill-title'];
  $billPaidDate = $_POST['bill-paid-date'];
  $billDate = time();
  $billOption = $_POST['bill-option'];
  $billDescriptions = $_POST['bill-description'];
  $billAmounts = $_POST['bill-amount'];
  $billPrices = $_POST['bill-price'];

  // Load clients from JSON file
  $data = json_decode(file_get_contents("data.json"), true);

  // Find the client with the matching ID
  $selectedClient = null;

  foreach ($data as &$client) {
    if ($client['id'] == $clientId) {
      $existingBillIndex = null;
      // Check if the bill with the given ID already exists
      foreach ($client["bills"] as $index => $bill) {
        if ($bill["id"] == $billId) {
          $existingBillIndex = $index;
          break;
        }
      }
      // Update existing bill or create a new one
      if ($existingBillIndex !== null) {
        // Update existing bill
        $selectedBill = &$client["bills"][$existingBillIndex];
        $selectedBill['title'] = $billTitle;
        $selectedBill['status'] = $billPaidDate;
        $selectedBill['date'] = $billDate;
        $selectedBill['status'] = $billPaidDate;

        // Update bill lines
        $billLines = [];
        for ($i = 0; $i < count($billDescriptions); $i++) {
          $billLine = [
            'description' => $billDescriptions[$i],
            'option' => $billOption[$i],
            'amount' => floatval($billAmounts[$i]),
            'price' => floatval($billPrices[$i])
          ];
          $billLines[] = $billLine;
        }
        $selectedBill['bill_lines'] = $billLines;
      } else {
        // Create new bill
        $newBillId = $billId ?: (lastID($data) + 1);
        $newBill = [
          'id' => $newBillId,
          'title' => $billTitle,
          'status' => $billPaidDate,
          'date' => $billDate,
          'status' => $billPaidDate
        ];
        // Create an array of bill lines
        $billLines = [];
        for ($i = 0; $i < count($billDescriptions); $i++) {
          $billLine = [
            'description' => $billDescriptions[$i],
            'option' => $billOption[$i],
            'amount' => floatval($billAmounts[$i]),
            'price' => floatval($billPrices[$i])
          ];
          $billLines[] = $billLine;
        }
        $newBill['bill_lines'] = $billLines;
        // Add new bill to the client
        $client["bills"][] = $newBill;
        $selectedBill = $newBill;
      }
      $selectedClient = $client;
      break;
    }
}

  // Save updated client data back to the JSON file
  file_put_contents("data.json", json_encode($data));

  // Return the updated client details as JSON response
  header('Content-Type: application/json');
  echo json_encode($selectedBill);
}
