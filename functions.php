<?php
function lastID($data, &$highestId=0) {
  foreach ($data as $key => $value) {
    if (is_array($value) || is_object($value)) {
      lastID($value, $highestId);
    } elseif ($key === 'id' && is_numeric($value)) {
      $id = intval($value);
      if ($id > $highestId) {
        $highestId = $id;
      }
    }
  }
  return $highestId;
}

function findElement($jsonObj, $key, $value) {
  if (is_array($jsonObj)) {
    foreach ($jsonObj as $item) {
      $result = findElement($item, $key, $value);
      if ($result !== null) {
          return $result;
      }
    }
  } elseif (is_object($jsonObj)) {
    foreach ($jsonObj as $k => $v) {
      if ($k === $key && $v === $value) {
          return $jsonObj;
      }
    }
    foreach ($jsonObj as $k => $v) {
      $result = findElement($v, $key, $value);
      if ($result !== null) {
          return $result;
      }
    }
  }

  return null;
}
