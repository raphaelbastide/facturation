<?php
include "functions.php";

if (isset($_GET['id'])) {
  $billId = $_GET['id'];
}

// Load clients from JSON file
$data = json_decode(file_get_contents("data.json"), true);

foreach ($data as $key => $client) {
  foreach ($client['bills'] as $billKey => $bill) {
    if ($bill['id'] == $billId) {
      unset($data[$key]['bills'][$billKey]);
    }
  }
}

// Save updated client list to JSON file
file_put_contents("data.json", json_encode($data));

// Send success response
http_response_code(200);

// Return the updated client details as JSON response
header('Content-Type: application/json');
echo json_encode($data);
