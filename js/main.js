let d = document
let b = d.body
const newBillForm = d.getElementById("new-bill-form")

resetClientForm()

// Load client details on page load
const clientDropdown = d.getElementById("client-dropdown")
clientDropdown.addEventListener("change", () => {
  resetClientForm()
  const clientId = clientDropdown.value
  const request = new XMLHttpRequest()
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      const client = JSON.parse(request.responseText)
      displayClientDetails(client)
    }
  }
  request.open("GET", `get-client.php?id=${clientId}`, true)
  request.send()
})

// Display client details
const billSelectContainer = d.querySelector(".bill-select")
function displayClientDetails(client) {
  const clientDetails = d.getElementById("client-details")
  const billClientId = d.getElementById("bill-client-id")
  let html = `<p class="en">Bill addressed to: </p><p class="fr">Facture adressée à : </p>`
    html += `<h2>${client.name}</h2>`
    html += `<p><b class="en">Address:</b><b class="fr">Adresse :</b> ${client.address}</p>`
    html += `<p><b>Email:</b> ${client.email}</p>`
    html += `<p><b>Siret:</b> ${client.siret}</p>`
    clientDetails.innerHTML = html
  billClientId.value = client.id

  const billDropdown = d.createElement("select")
  billDropdown.id = "bill-dropdown"
  
  let bills = client.bills
  let firstOption = d.createElement("option")
  firstOption.innerText = "Select Bill"
  billSelectContainer.innerHTML = ""
  billDropdown.appendChild(firstOption)
  billSelectContainer.appendChild(billDropdown)
  bills.forEach(bill => {
    let option = d.createElement("option")
    option.innerText = bill.title
    option.value = bill.id
    billDropdown.appendChild(option)
  })

  billDropdown.addEventListener("change", () => {
    const billId = billDropdown.value
    const request = new XMLHttpRequest()
    request.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        const bill = JSON.parse(request.responseText)
        displayBillDetails(bill, client)
      }
    }
    request.open("GET", `get-bill.php?id=${billId}`, true)
    request.send()
  })
}

// Display client form
function resetClientForm(){
  let clientHTML = `<input hidden class="b-1-2" id="bill-client-id" placeholder="Client ID" type="number" name="client-id"><input hidden class="b-1-2" id="bill-id" placeholder="Bill ID" type="number" name="bill-id">
  <div class="line">
    <input class="b-1-2" id="bill-title" type="text" placeholder="Bill title" name="bill-title">
    <input type="date" class="b-1-2" id="bill-paid-date" name="bill-paid-date" placeholder="Bill payment date"/>
  </div>
  <div id="bill-line-container">
  </div>`
  d.querySelector('.client-form').innerHTML = clientHTML
  addBillLine("", 1, "", false)
}

// Display bill details
const billDetails = d.getElementById("bill-details")
function displayBillDetails(bill, client) {
  // const billId = d.getElementById("bill-id")
  let editBtn = d.createElement('button')
  editBtn.onclick = function(){editBill(bill,client)} 
  editBtn.innerHTML = "Edit bill"
  let printBtn = d.createElement('button')
  printBtn.onclick = function(){window.print()}
  printBtn.innerHTML = "Print bill"
  let deleteBtn = d.createElement('button')
  deleteBtn.onclick = function(){deleteBill(bill)} 
  deleteBtn.innerHTML = "Delete bill"
  let date = new Date(bill.date*1000).toLocaleDateString();
  let paid = bill.paid_date?bill.paid_date:"unpaid"
  let html = `<header><h2>${bill.title}</h2>`
  html += `<p class='bill-paid-date noprint'>Payée: ${paid}</p>`
  html += `<p><b class="fr">Facture n° </b><b class="en">Bill #</b>${bill.id}</p>`
  d.title = `bill-${bill.id}-${client.name}`
  html += `<p>Date: ${date}</p></header>`
  let total = 0
  for (let i = 0; i < bill.bill_lines.length; i++) {
    let option = bill.bill_lines[i].option=="true"?"option":""
    html += `<div class="line ${option}">` 
    html += `<p class="line b-6-8">${bill.bill_lines[i].description}</p>`
    html += `<p class="line b-1-8 a-right">${bill.bill_lines[i].amount}</p>`
    html += `<p class="line b-1-8 a-right">${bill.bill_lines[i].price}€</p>`
    html += `</div>` // end .line 
    if (bill.bill_lines[i].option == "false") {      
      total += bill.bill_lines[i].price
    }
  }
  total = fixTotal(total)
  html += `<p class="line total b-1-1"><span>Total HT:</span> <span>${total}€</span></p>`
  html += `<p class="vat-line fr b-1-1">TVA non applicable, art. 293B du CGI</p>
  <p class="vat-line en b-1-1">VAT not applicable, art. 293B of the CGI</p>`
  billDetails.innerHTML = html
  billDetails.appendChild(editBtn)
  billDetails.appendChild(printBtn)
  billDetails.appendChild(deleteBtn)
}

function editBill(bill,client){
  d.getElementById("bill-title").value = bill.title
  d.getElementById("bill-paid-date").value = bill.paid_date
  d.getElementById("bill-client-id").value = client.id
  d.getElementById("client-dropdown").value = client.id
  d.getElementById("bill-id").value = bill.id
  d.getElementById("bill-line-container").innerHTML = ""
  bill.bill_lines.forEach(line => {
    addBillLine(line.description, line.amount, line.price, line.option)
  })
  getTotal()
}

function deleteBill(bill) {
  let billId = bill.id
  const request = new XMLHttpRequest()
  request.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      location.reload()
    }
  };
  request.open("POST", `delete-bill.php?id=${billId}`, true)
  request.send()
}


// Handle form submission for new clients
const newClientForm = d.getElementById("new-client-form")
newClientForm.addEventListener("submit", event => {
  event.preventDefault()
  const nameInput    = d.getElementById("new-client-name")
  const addressInput = d.getElementById("new-client-address")
  const emailInput   = d.getElementById("new-client-email")
  const siretInput   = d.getElementById("new-client-siret")
  const newClient    = {
    name: nameInput.value,
    address: addressInput.value,
    email: emailInput.value,
    siret: siretInput.value
  }
  const request = new XMLHttpRequest()
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      // Reload page to update client dropdown
      location.reload()
    }
  }
  request.open("POST", "create-client.php", true)
  request.setRequestHeader("Content-Type", "application/jsoncharset=UTF-8")
  request.send(JSON.stringify(newClient))
})


// New bill
newBillForm.addEventListener("submit", event => {
  event.preventDefault()
  
  // if existing bill
  const billId           = d.getElementById('bill-id').value
  const clientId         = d.getElementById('bill-client-id').value
  const billTitle        = d.getElementById('bill-title').value
  const billPaidDate     = d.getElementById('bill-paid-date').value
  const billDescriptions = d.getElementsByName('bill-description[]')
  const billOption       = d.getElementsByName('bill-option[]')
  const billAmounts      = d.getElementsByName('bill-amount[]')
  const billPrices       = d.getElementsByName('bill-price[]')
  
  const request = new XMLHttpRequest()
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log(request.responseText);
      const bill = JSON.parse(request.responseText)
      displayBillDetails(bill, clientId)
      d.getElementById('new-bill-form').reset()
    }
  }
  request.open("POST", `save-bill.php`, true)
  request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
  
  let formData = `client-id=${clientId}`
  formData += `&bill-title=${billTitle}`
  formData += `&bill-paid-date=${billPaidDate}`
  formData += `&bill-id=${billId}`
  for (let i = 0; i < billDescriptions.length ; i++) {
    formData += `&bill-description[]=${billDescriptions[i].value}`
    formData += `&bill-option[]=${billOption[i].checked}`
    formData += `&bill-amount[]=${billAmounts[i].value}`
    formData += `&bill-price[]=${billPrices[i].value}`
  }
  request.send(formData)
})

// Function to add a new bill line
function addBillLine(desc,amount,price,option) {
  const line = d.createElement('div')
  line.classList.add("bill-line")
  line.classList.add("line")

  // Create new input fields for description and amount
  const optionLabel = d.createElement("label")
  const optionInput = d.createElement("input")
  optionInput.setAttribute("type", "checkbox")
  optionInput.setAttribute("name", "bill-option[]")
  optionLabel.classList.add("b-1-6", "option-checkbox")
  if(option=="true"){
    optionInput.checked = "checked"
    line.classList.add('option')
  }
  // optionLabel.insertAdjacentText("afterbegin", "Option")
  optionLabel.appendChild(optionInput)
  
  const descriptionInput = d.createElement("textarea")
  descriptionInput.setAttribute("name", "bill-description[]")
  descriptionInput.classList.add("b-3-6")
  if(desc) descriptionInput.innerText = desc

  const amountInput = d.createElement("input")
  amountInput.setAttribute("type", "number")
  amountInput.setAttribute("name", "bill-amount[]")
  amountInput.setAttribute("value", "1")
  amountInput.setAttribute("placeholder", "Amount")
  amountInput.onchange = function(){getTotal()}
  amountInput.classList.add("b-1-6")
  amountInput.classList.add("amount")
  if(amount) amountInput.value = amount
  
  const priceInput = d.createElement("input")
  priceInput.setAttribute("type", "number")
  priceInput.setAttribute("step", ".5")
  priceInput.setAttribute("name", "bill-price[]")
  priceInput.setAttribute("placeholder", "Price")
  priceInput.onchange = function(){getTotal()}
  priceInput.classList.add("b-1-6")
  priceInput.classList.add("price")
  if(price) priceInput.value = price
  
  // Append the new input fields to the container
  line.appendChild(optionLabel)
  line.appendChild(descriptionInput)
  line.appendChild(amountInput)
  line.appendChild(priceInput)
  const billLineContainer = d.getElementById("bill-line-container")
  billLineContainer.appendChild(line)
  bindOptionClick(line)
}

function getTotal(){
  let billTotal = 0
  let lines = d.querySelectorAll('.bill-line')
  lines.forEach(line => {
    let amount = line.querySelector('.amount')
    let price = line.querySelector('.price')
    if (!line.classList.contains("option")) {
      billTotal += amount.value * price.value
    }
  })
  billTotal = fixTotal(billTotal)
  d.querySelector('.edit-total span').innerHTML = billTotal
}

let langBtns = d.querySelectorAll('.lang-buttons button')
langBtns.forEach(btn => {
  btn.onclick = function(){
    let lang = btn.dataset.lang
    console.log(lang);
    b.dataset.lang = lang
  }
});

function bindOptionClick(el){
  el.querySelector('.option-checkbox').onclick = function(){
    if (el.classList.contains('option')) {
      el.classList.remove('option')
    }else{
      el.classList.add('option')
    }
  }
}

function fixTotal(t){
  return t.toFixed(2).replace(/\.00$/, '')
}