# Facta, a very simple billing app

- Requires php
- No db required other than a json file
- Rename `default-data.json` to `data.json`

## Features

- Clients / bills
- Exports PDF with CSS print (CTRL+P)
- Very basic multilingual options
- Optional bill lines
- Bill edit / print / delete functions

## License

[GNU AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html)

![Assisted by AI](https://ai-label.org/image-pack/ai-label_banner-assisted-by-ai.svg)  
AI generated code were involved in the making of this app.