<?php
include "functions.php";

if (isset($_GET['id'])) {
    $billId = $_GET['id'];

    // Load bills from JSON file
    $data = json_decode(file_get_contents("data.json"), true);
    
    $selectedBill = null;
    foreach ($data as $client) {
      $bills = $client["bills"];
      foreach ($bills as $bill) {
        if ($bill['id'] == $billId) {
          $selectedBill = $bill;
          break;
        }
      }
    }  
    // Check if the bill was found
    if ($selectedBill) {
      // Return the bill details as JSON response
      header('Content-Type: application/json');
      echo json_encode($selectedBill); 
    } else {
      // Return an error response if the bill was not found
      http_response_code(404);
      echo json_encode(['error' => 'Bill not found']);
    }
} else {
  // Return an error response if the ID is not provided
  http_response_code(400);
  echo json_encode(['error' => 'Bill ID is missing']);
}
