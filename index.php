<?php include "functions.php"; ?>
<!DOCTYPE html>
<html>
<head>
  <title>Facta</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="fonts/stylesheet.css">
  <link rel="shortcut icon" href="favicon.svg" type="image/x-icon">
</head>
<body data-lang="en">
  <header class="b-c toprint">
    <div class="lang-buttons b-1-1">
      <button class="en" data-lang="fr" autocomplete="off">Français</button>
      <button class="fr" data-lang="en" autocomplete="off">English</button>
    </div>
    <div class="b-1-2">
      <p>Studio Raphaël Bastide</p>
      <p>33 rue Lebour <br> 93100 Montreuil, France</p>
    </div>
    <div class="b-1-2">
      <p>Web: &nbsp; <a href="https://raphaelbastide.com">https://raphaelbastide.com</a></p>
      <p>Email: <a href="mailto:bonjour@raphaelbastide.com">bonjour@raphaelbastide.com</a></p>
      <p>Tel.:&nbsp; +33 684313994</p>
    </div>
  </header>
  <main class="b-c">
    <details class="b-1-3 noprint">
      <summary>New client</summary>
      <form id="new-client-form">
          <input type="text" placeholder="Name" id="new-client-name" name="name" required><br>
          <input type="text" placeholder="Adress" id="new-client-address" name="address"><br>
          <input type="email" placeholder="Email" id="new-client-email" name="email" required><br>
          <input type="text" placeholder="Siret" id="new-client-siret" name="siret"><br>
          <button type="submit">Create Client</button>
        </form>
    </details>
    <form id="new-bill-form" class="b-1-1 b-c noprint">
      <div class="client-select b-1-2">
        <select class="line" id="client-dropdown" autocomplete="off" required>
        <?php
          // Load clients from JSON file
          $clients = json_decode(file_get_contents("data.json"), true);
          echo "<option value=''>Select client</option>";
          // Output option for each client
          foreach ($clients as $client) {
            echo "<option value='{$client['id']}'>{$client['name']}</option>";
          }
        ?>
        </select>
      </div>
      <div class="bill-select b-1-2"> <!-- placeholder for bill selector --> </div> 
      <div class="client-form b-1-1"> <!-- placeholder for bill form --></div>
      <button type="button" onclick="addBillLine()">Add Line</button>
      <p class="edit-total line b-1-1">Total HT = <span>0</span></p>
      <button type="submit">Save Bill</button>
    </form>
    <div class="b-1-1 toprint" id="client-details"></div>
    <div class="b-1-1 toprint" id="bill-details"></div>
    <div class="b-1-1 toprint" id="bill-custom-text" contenteditable>—</div>
  </main>
  <footer class="b-c toprint">
    <div class="b-1-1 b-c details">
      <p class="b-1-2"><span class="fr">Mes coordonnées bancaires pour règlement par virement :</span> <span class="en"> My bank details for payment by transfer:</span></p>
      <div class="bank-details b-1-2">
        <p>Raphaël Bastide</p>
        <p>RIB:&nbsp; 42559 10000 04130676086 96<br>
  IBAN: FR76 4255 9100 0004 1306 7608 696<br>
  BIC:&nbsp; CCOPFRPPXXX<br>
  <span class="fr">Banque</span><span class="en">Bank:</span> Crédit Coopératif<br></p>
      </div>
    </div>
    <div class="footer-infos b-1-1 b-c">
      <p class="b-1-2">Studio Raphaël Bastide <br> Entreprise Individuelle <br> Siret: 507 940 310 00034  <br> APE: &nbsp; 9003A</p>
      <p class="b-1-2"><i>Dispensé d'immatriculation au Registre du Commerce et des Sociétés et au Répertoire des Métiers.<br>Clause de réserve de propriété : le transfert de propriété de la chose vendue est subordonné au paiement du prix à l'échéance par le client.</i></p>
    </div>
  </footer>
  <script src="js/main.js"></script>
</body>
</html>
