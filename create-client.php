<?php
include "functions.php";

// Get new client data from request body
$newClientData = json_decode(file_get_contents("php://input"), true);

// Load clients from JSON file
$data = json_decode(file_get_contents("data.json"), true);

// Generate new client ID
$newClientId = lastID($data) + 1;

// Add new client to list
$newClient = [
    'id' => $newClientId,
    'name' => $newClientData['name'],
    'address' => $newClientData['address'],
    'email' => $newClientData['email'],
    'siret' => $newClientData['siret'],
    'bills' => []
];
array_push($data, $newClient);

// Save updated client list to JSON file
file_put_contents("data.json", json_encode($data));

// Send success response
http_response_code(200);
echo json_encode(['message' => 'Client created successfully.']);
