<?php
if (isset($_GET['id'])) {
  $clientId = $_GET['id'];

  // Load clients from JSON file
  $clients = json_decode(file_get_contents("data.json"), true);

  // Find the client with the matching ID
  $selectedClient = null;
  foreach ($clients as $client) {
    if ($client['id'] == $clientId) {
      $selectedClient = $client;
      break;
    }
  }

  // Check if the client was found
  if ($selectedClient) {
    // Return the client details as JSON response
    header('Content-Type: application/json');
    echo json_encode($selectedClient);
  } else {
    // Return an error response if the client was not found
    http_response_code(404);
    echo json_encode(['error' => 'Client not found']);
  }
} else {
  // Return an error response if the ID is not provided
  http_response_code(400);
  echo json_encode(['error' => 'Client ID is missing']);
}
